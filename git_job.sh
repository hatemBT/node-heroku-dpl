#!/bin/bash

current_branch=$(git branch | grep \* | cut -d ' ' -f2)

echo -e "\n\e[0;31m Adding files to the index ..\n"
git add -A
sleep 1

echo -e "\n\e[0;32mCommit changes with : $1\n"
git commit -m "$1"

echo -e " \n\e[0;33mpushing to current branche : $current_branch\n"
git push origin $current_branch

